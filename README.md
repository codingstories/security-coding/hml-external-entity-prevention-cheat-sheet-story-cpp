# XML Injection Story

**To read**: <https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html>

**Estimated reading time**: 15 min

## Story Outline

This story is focused on providing guidance for XML external entity prevention cheat sheet flaws
in your applications.

## Story Organization

**Story Branch**: main
> `git checkout main`

**Practical task tag for self-study**: task
>`git checkout task`

Tags: #cpp, #xml, #security
