#include "LocalStorage.h"
#include "XmlController.h"
#include <iostream>

namespace xml_inj {

    LocalStorage::LocalStorage() :
        m_users({
            {"user01", "bitnam1"},
            {"user02", "bitnam2"}
        }) {
    }

    bool LocalStorage::hasUser(std::string_view login, std::string_view password) const {
        // emulate that we get XML
        XMLController ctrl;
        const std::string xml = ctrl.create(login, password);
        Creds cred = ctrl.parse(xml);
        if ( password.empty() ) // just to see output
            std::cout << "user: " << cred.user << std::endl; 
        auto result = m_users.find(cred.user);
        if (result != m_users.end()) {
            auto&[log, pass] = *result;
            return (password.empty()? (cred.user == log): (cred.user  == log and cred.pass == pass));
        }
        return  false;
    }

    StoragePtr createStorage() {
        return std::make_unique<LocalStorage>();
    }
} // namespace xml_inj
